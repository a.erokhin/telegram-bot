# @ZaHlebomBot

[@ZaHlebomBot](https://t.me/zaHlebomBot?start) - бот для ведения совместных списков. Пользователи могут создавать списки покупок, домашних дел или любые другие списки на которые хватит фантазии.

## Installation

Для запуска проекта необходимо:

- Склонировать репозиторий
- Добавить в переменные 
  - TELEGRAM_BOT_USERNAME
  - TELEGRAM_BOT_TOKEN
- Собрать проект выполнив `maven install`

## Usage

Перейдите в бота и используйте.

## Contributing

Pull requests are welcome. Для серьезных изменений, пожалуйста, сначала откройте вопрос, чтобы обсудить, что бы вы хотели изменить.

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)




