package blog.aedev.zahlebom.dto;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class ListInfoDto {

    private long id;

    private String name;

    private UserDto owner;

    private boolean isSelected;

    private boolean isCurrentUserOwner;

    private Long userId;

    private Set<UserDto> users;
}
