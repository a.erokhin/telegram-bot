package blog.aedev.zahlebom.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "list")
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class ToDoList {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceListGenerator")
    @SequenceGenerator(name = "sequenceListGenerator", sequenceName = "sequence_list_generator", allocationSize = 1)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToMany
    @JoinTable(
        name = "user_list",
        joinColumns = {@JoinColumn(name = "list_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")})
    @Builder.Default
    private Set<UserInfo> users = new HashSet<>();

    @NotNull
    @ManyToOne(optional = false)
    @JoinColumn(name="owner_user_id", nullable = false, updatable = false)
    private UserInfo owner;

    @Column(name = "deleted")
    @Builder.Default
    private boolean deleted = false;

    @OneToMany(mappedBy = "list")
    @Builder.Default
    private List<Item> items = new ArrayList<>();

    @Embedded
    @Builder.Default
    private Audit audit = new Audit();

    @Transient
    @Builder.Default
    private boolean isSelectForCurrentUser = false;
}
