package blog.aedev.zahlebom.model;

import java.time.OffsetDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "delete_item")
@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class DeleteItem {

    @Id
    private Long id;

    @NotNull
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Product product;

    @NotNull
    @ManyToOne(optional = false)
    private ToDoList list;

    @Column(name = "purchased")
    private boolean purchased;

    @Column(name = "created_date", updatable = false, nullable = false)
    private OffsetDateTime createdDate;

    @Column(name = "last_modified_by", nullable = false)
    private Long lastModifiedBy;

    @Column(name = "last_modified_date", nullable = false)
    private OffsetDateTime lastModifiedDate;

    @Column(name = "created_by", updatable = false, nullable = false)
    private Long createdBy;

    @CreatedBy
    @Column(name = "deleted_by")
    private Long deletedBy;

    @CreatedDate
    @Column(name = "deleted_date")
    private OffsetDateTime deletedDate;
}
