package blog.aedev.zahlebom.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "token")
@Getter
@Setter
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Token {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceTokenGenerator")
    @SequenceGenerator(name = "sequenceTokenGenerator", sequenceName = "sequence_token_generator", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "value")
    private String value;

    @NotNull
    @Column(name = "expiration_date")
    private LocalDateTime expirationDate;

    @ManyToOne(optional = false)
    private ToDoList list;

    @Embedded
    @Builder.Default
    private Audit audit = new Audit();
}
