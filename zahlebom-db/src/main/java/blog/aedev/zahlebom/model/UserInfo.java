package blog.aedev.zahlebom.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@NamedEntityGraph(
    name = "user.list.items.product",
    attributeNodes = {
        @NamedAttributeNode(value = "selectList", subgraph = "list-subgraph")
    },
    subgraphs = {
        @NamedSubgraph(
            name = "list-subgraph",
            attributeNodes = {
                @NamedAttributeNode(value = "items", subgraph = "product-subgraph")
            }
        ),
        @NamedSubgraph(
            name = "product-subgraph",
            attributeNodes = {
                @NamedAttributeNode(value = "product")
            }
        )
    }
)

@Entity
@Table(name = "user_info")
@Getter
@Setter
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "username")
    private String username;

    @Column(name = "language_code")
    private String languageCode;

    @Column(name = "enabled")
    @Builder.Default
    private boolean enabled = false;

    @ManyToOne
    private ToDoList selectList;

    @Transient
    @Builder.Default
    private boolean isNew = false;

    @Embedded
    @Builder.Default
    private Audit audit = new Audit();
}
