package blog.aedev.zahlebom.controller;

import blog.aedev.zahlebom.config.ApplicationProperties;
import blog.aedev.zahlebom.service.MessageService;
import java.util.Set;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
@RequiredArgsConstructor
public class Bot extends TelegramLongPollingCommandBot {

    private final ApplicationProperties applicationProperties;
    private final MessageService messageService;
    private final Set<BotCommand> commands;

    @PostConstruct
    public void registerCommand() {
        commands.forEach(this::register);
    }

    @Override
    public void processNonCommandUpdate(Update update) {
        messageService.execute(update, this);
    }

    @Override
    public String getBotUsername() {
        return applicationProperties.getTelegramBot().getUsername();
    }

    @Override
    public String getBotToken() {
        return applicationProperties.getTelegramBot().getToken();
    }
}
