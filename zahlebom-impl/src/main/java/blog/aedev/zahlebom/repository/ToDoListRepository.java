package blog.aedev.zahlebom.repository;

import blog.aedev.zahlebom.model.ToDoList;
import blog.aedev.zahlebom.model.UserInfo;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ToDoListRepository extends JpaRepository<ToDoList, Long> {

    List<ToDoList> findAllByUsersContainsAndDeletedIsFalse(UserInfo userInfo);

    Optional<ToDoList> findByIdAndUsersContainsAndDeletedIsFalse(Long id, UserInfo userInfo);

    @Query("SELECT user.selectList.id FROM UserInfo user WHERE user.id = :userId")
    Optional<Long> findIdOfCurrentList(@Param("userId") Long userId);
}
