package blog.aedev.zahlebom.repository;

import blog.aedev.zahlebom.model.UserInfo;
import java.util.Optional;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<UserInfo, Long> {

    @EntityGraph(value = "user.list.items.product")
    @Query("SELECT u FROM UserInfo u WHERE u.id = :id")
    Optional<UserInfo> findOneByIdWithEntityGraph(@Param("id") Long id);
}
