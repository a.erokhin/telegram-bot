package blog.aedev.zahlebom.repository;

import blog.aedev.zahlebom.model.Item;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item, Long> {

    List<Item> findAllByListIdAndPurchased(Long listId, boolean purchased);

    List<Item> findAllByListId(Long listId);
}
