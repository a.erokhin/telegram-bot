package blog.aedev.zahlebom.repository;

public class ThreadLocalStorage {

    private static final ThreadLocal<Long> userId = new ThreadLocal<>();

    public static void setUserId(Long id) {
        userId.set(id);
    }

    public static Long getUserId() {
        return userId.get();
    }
}
