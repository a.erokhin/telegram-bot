package blog.aedev.zahlebom.repository;

import blog.aedev.zahlebom.model.DeleteItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DeleteItemRepository extends JpaRepository<DeleteItem, Long> {

    @Query(value = "select di from DeleteItem di where di.list.id = :listId order by di.deletedDate desc")
    Page<DeleteItem> findByListIdAndLastModified(@Param("listId") Long listId, Pageable pageable);
}
