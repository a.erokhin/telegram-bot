package blog.aedev.zahlebom.listener;

import blog.aedev.zahlebom.repository.ThreadLocalStorage;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Component
public class MessageListenerManager {

    private final Map<Long, MessageListener> listeners = new ConcurrentHashMap<>();

    public void subscribe(MessageListener messageListener) {
        listeners.put(ThreadLocalStorage.getUserId(), messageListener);
    }

    public void unSubscribe() {
        listeners.remove(ThreadLocalStorage.getUserId());
    }

    public boolean notify(Update update, AbsSender sender) {
        User telegramUser = update.getMessage().getFrom();

        Optional<MessageListener> listener = Optional.ofNullable(listeners.get(telegramUser.getId()));
        if (listener.isPresent()) {
            listener.get().update(update, sender);
            return true;
        }
        return false;
    }
}
