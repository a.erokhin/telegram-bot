package blog.aedev.zahlebom.listener;

import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.bots.AbsSender;

public interface MessageListener {
    void setObjectId(Long id);

    void update(Update update, AbsSender sender);
}
