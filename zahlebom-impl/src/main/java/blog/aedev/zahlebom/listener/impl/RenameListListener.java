package blog.aedev.zahlebom.listener.impl;

import static blog.aedev.zahlebom.command.util.MessageUtil.makeMessage;
import static blog.aedev.zahlebom.config.ThymeleafConstants.RENAME_FINISH_TEMPLATE;
import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import blog.aedev.zahlebom.exception.ExceptionHandler;
import blog.aedev.zahlebom.listener.MessageListener;
import blog.aedev.zahlebom.service.ToDoListService;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Slf4j
@Scope(SCOPE_PROTOTYPE)
@Component
@Setter
@RequiredArgsConstructor
public final class RenameListListener implements MessageListener {

    private final ToDoListService listService;
    private final SpringTemplateEngine templateEngine;

    private Long objectId;

    @Override
    public void update(Update update, AbsSender sender) {
        Chat chat = update.getMessage().getChat();
        try {
            String message = update.getMessage().getText().trim();

            listService.rename(message, objectId);

            Context context = new Context();
            sender.execute(makeMessage(chat.getId(), templateEngine.process(RENAME_FINISH_TEMPLATE, context)));
        } catch (Exception e) {
            ExceptionHandler.handle(e, sender, chat.getId());
        }
    }
}
