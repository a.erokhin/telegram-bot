package blog.aedev.zahlebom.command.util;

import static blog.aedev.zahlebom.command.util.MenuUtil.makeMainMenu;
import static org.telegram.telegrambots.meta.api.methods.ParseMode.HTML;

import org.apache.commons.lang3.StringUtils;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;

public final class MessageUtil {

    public static final String DIV = "<\\/?div>";

    private MessageUtil() {
    }

    public static SendMessage makeMessage(Long chatId, String text) {
        text = text.replaceAll(DIV, StringUtils.EMPTY);
        return SendMessage.builder()
            .parseMode(HTML)
            .chatId(chatId.toString())
            .text(text)
            .disableWebPagePreview(false)
            .replyMarkup(makeMainMenu())
            .build();
    }

    public static SendMessage makeMessage(Long chatId, String text, ReplyKeyboard replyMarkup) {
        text = text.replaceAll(DIV, StringUtils.EMPTY);
        return SendMessage.builder()
            .parseMode(HTML)
            .chatId(chatId.toString())
            .text(text)
            .disableWebPagePreview(false)
            .replyMarkup(replyMarkup)
            .build();
    }

    public static EditMessageText makeEditMessage(Long chatId, Integer messageId, String text, InlineKeyboardMarkup replyMarkup) {
        text = text.replaceAll(DIV, StringUtils.EMPTY);
        return EditMessageText.builder()
            .parseMode(HTML)
            .chatId(chatId.toString())
            .text(text)
            .disableWebPagePreview(false)
            .replyMarkup(replyMarkup)
            .messageId(messageId)
            .build();
    }
}
