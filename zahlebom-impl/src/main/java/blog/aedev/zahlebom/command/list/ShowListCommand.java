package blog.aedev.zahlebom.command.list;

import static blog.aedev.zahlebom.config.MenuConstants.SHOW_LIST_COMMAND;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Component(SHOW_LIST_COMMAND)
@Slf4j
public class ShowListCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "show_list";

    private final ShowOrderedListCommand showOrderedListCommand;

    public ShowListCommand(ShowOrderedListCommand showOrderedListCommand) {
        super(COMMAND_IDENTIFIER, null);
        this.showOrderedListCommand = showOrderedListCommand;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        showOrderedListCommand.execute(sender, user, chat, args);
    }
}
