package blog.aedev.zahlebom.command.settings;

import static blog.aedev.zahlebom.command.util.MenuUtil.makeEditListMenu;
import static blog.aedev.zahlebom.command.util.MessageUtil.makeEditMessage;
import static blog.aedev.zahlebom.config.MainConstants.LIST_ID;
import static blog.aedev.zahlebom.config.MainConstants.MESSAGE_ID;
import static blog.aedev.zahlebom.config.MenuConstants.EDIT_LIST_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.LIST_ATTRIBUTE;
import static blog.aedev.zahlebom.config.ThymeleafConstants.LIST_TEMPLATE;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.dto.ListInfoDto;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(EDIT_LIST_COMMAND)
@Slf4j
public class EditListCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "edit_list";

    private final TelegramBotFacade telegramBotFacade;

    public EditListCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        Long listId = Long.parseLong(args.get(LIST_ID));
        ListInfoDto list = telegramBotFacade.getListInfoById(listId);
        Context context = new Context();
        context.setVariable(LIST_ATTRIBUTE, list);

        Integer messageId = Integer.parseInt(args.get(MESSAGE_ID));
        sender.execute(makeEditMessage(chat.getId(), messageId,
            templateEngine.process(LIST_TEMPLATE, context), makeEditListMenu(list)));
    }
}
