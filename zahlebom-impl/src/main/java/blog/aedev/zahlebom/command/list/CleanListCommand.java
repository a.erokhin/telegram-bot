package blog.aedev.zahlebom.command.list;

import static blog.aedev.zahlebom.command.util.MessageUtil.makeMessage;
import static blog.aedev.zahlebom.config.MenuConstants.CLEAN_LIST_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.CLEAN_TEMPLATE;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(CLEAN_LIST_COMMAND)
@Slf4j
public class CleanListCommand extends AbstractBotCommand {

    private final static String COMMAND_IDENTIFIER = "clean_list";

    private final TelegramBotFacade telegramBotFacade;

    public CleanListCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        telegramBotFacade.cleanList();
        sender.execute(makeMessage(chat.getId(), templateEngine.process(CLEAN_TEMPLATE, new Context())));
    }
}
