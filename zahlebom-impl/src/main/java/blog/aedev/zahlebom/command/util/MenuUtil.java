package blog.aedev.zahlebom.command.util;

import static blog.aedev.zahlebom.command.util.CommandUtil.withCounter;
import static blog.aedev.zahlebom.config.MenuConstants.CREATE_LIST_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.DELETE_LIST_WITH_LIST_ID_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.EDIT_LIST_WITH_LIST_ID_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.OPERATION_CANCEL_COMMAND_WITH_LIST_ID;
import static blog.aedev.zahlebom.config.MenuConstants.RENAME_LIST_WITH_LIST_ID_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.SELECT_LIST_WITH_LIST_ID_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.SETTINGS_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.SHARE_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.SHARE_WITH_LIST_ID_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.SHOW_LIST_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.SHOW_LIST_USERS_WITH_LIST_ID_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.UNSUBSCRIBE_WITH_LIST_ID_AND_USER_ID_COMMAND;

import blog.aedev.zahlebom.dto.ItemDto;
import blog.aedev.zahlebom.dto.ListDto;
import blog.aedev.zahlebom.dto.ListInfoDto;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

public final class MenuUtil {

    private static final String BACK_BUTTON = "⬅️ назад";
    private static final String SHARE_BUTTON = "\uD83D\uDC6A Поделиться";
    private static final String CANCEL_INVITE_BUTTON = "❌ Отозвать";
    private static final String RENAME_BUTTON = "✏️ Переименовать";
    private static final String SELECT_BUTTON = "✔️ Выбрать";
    private static final String DELETE_BUTTON = "\uD83D\uDDD1️ Удалить";
    private static final String CANCEL_BUTTON = "↪️️ Отменить";
    private static final String UNSUBSCRIBE_BUTTON = "❌ Отписаться";
    private static final String CHECKMARK = "✅ ";
    private static final String DOT = ". ";
    private static final String CREATE_BUTTON = "\uD83C\uDD95 новый список";
    private static final String EMPTY = "";

    private MenuUtil() {
    }

    public static ReplyKeyboardMarkup makeMainMenu() {
        ArrayList<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow firstRow = new KeyboardRow();
        KeyboardRow secondRow = new KeyboardRow();
        keyboard.add(firstRow);
        keyboard.add(secondRow);

        firstRow.add(SHOW_LIST_COMMAND);
        secondRow.add(SHARE_COMMAND);
        secondRow.add(SETTINGS_COMMAND);

        return ReplyKeyboardMarkup.builder()
            .selective(false)
            .resizeKeyboard(true)
            .oneTimeKeyboard(false)
            .keyboard(keyboard)
            .build();
    }

    public static InlineKeyboardMarkup makeBackButton() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        inlineKeyboardMarkup.setKeyboard(keyboard);

        InlineKeyboardButton backButton = InlineKeyboardButton.builder()
            .text(BACK_BUTTON)
            .callbackData(SETTINGS_COMMAND)
            .build();

        keyboard.add(List.of(backButton));
        return inlineKeyboardMarkup;
    }

    public static InlineKeyboardMarkup makeEditListMenu(ListInfoDto listInfoDto) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        inlineKeyboardMarkup.setKeyboard(keyboard);

        if (!listInfoDto.isSelected()) {
            InlineKeyboardButton selectButton = InlineKeyboardButton.builder()
                .text(SELECT_BUTTON)
                .callbackData(String.format(SELECT_LIST_WITH_LIST_ID_COMMAND, listInfoDto.getId()))
                .build();
            keyboard.add(List.of(selectButton));
        }

        if (listInfoDto.isCurrentUserOwner()) {
            InlineKeyboardButton editNameButton = InlineKeyboardButton.builder()
                .text(RENAME_BUTTON)
                .callbackData(String.format(RENAME_LIST_WITH_LIST_ID_COMMAND, listInfoDto.getId()))
                .build();

            InlineKeyboardButton deleteButton = InlineKeyboardButton.builder()
                .text(DELETE_BUTTON)
                .callbackData(String.format(DELETE_LIST_WITH_LIST_ID_COMMAND, listInfoDto.getId()))
                .build();

            InlineKeyboardButton shareButton = InlineKeyboardButton.builder()
                .text(SHARE_BUTTON)
                .callbackData(String.format(SHARE_WITH_LIST_ID_COMMAND, listInfoDto.getId()))
                .build();

            InlineKeyboardButton cancelInviteButton = InlineKeyboardButton.builder()
                .text(CANCEL_INVITE_BUTTON)
                .callbackData(String.format(SHOW_LIST_USERS_WITH_LIST_ID_COMMAND, listInfoDto.getId()))
                .build();


            keyboard.add(List.of(shareButton, cancelInviteButton));
            keyboard.add(List.of(editNameButton, deleteButton));
        } else {
            InlineKeyboardButton unsubscribeButton = InlineKeyboardButton.builder()
                .text(UNSUBSCRIBE_BUTTON)
                .callbackData(String.format(UNSUBSCRIBE_WITH_LIST_ID_AND_USER_ID_COMMAND, listInfoDto.getId(), listInfoDto.getUserId()))
                .build();
            keyboard.add(List.of(unsubscribeButton));
        }

        InlineKeyboardButton backButton = InlineKeyboardButton.builder()
            .text(BACK_BUTTON)
            .callbackData(SETTINGS_COMMAND)
            .build();

        keyboard.add(List.of(backButton));
        return inlineKeyboardMarkup;
    }

    public static InlineKeyboardMarkup makeItemsMenu(ListDto list, String callbackData) {
        return makeItemsMenu(list, callbackData, false);
    }

    public static InlineKeyboardMarkup makeItemsMenu(ListDto list, String callbackData, boolean isShowCancelButton) {
        InlineKeyboardMarkup inlineKeyboardMarkup = null;
        if (list.getItems().size() > 0) {
            inlineKeyboardMarkup = new InlineKeyboardMarkup();
            List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
            inlineKeyboardMarkup.setKeyboard(keyboard);

            list.getItems().forEach(withCounter((i, item) -> {
                String text = item.getIsPurchased() ? CHECKMARK : StringUtils.EMPTY;
                text += (i + 1) + DOT + item.getProductName();

                InlineKeyboardButton button = InlineKeyboardButton.builder()
                    .text(text)
                    .callbackData(String.format(callbackData, item.getId()))
                    .build();
                keyboard.add(List.of(button));
            }));

            if (isShowCancelButton) {
                InlineKeyboardButton cancelButton = InlineKeyboardButton.builder()
                    .text(CANCEL_BUTTON)
                    .callbackData(String.format(OPERATION_CANCEL_COMMAND_WITH_LIST_ID, list.getItems().get(0).getListId()))
                    .build();
                keyboard.add(List.of(cancelButton));
            }

            InlineKeyboardButton button = InlineKeyboardButton.builder()
                .text(BACK_BUTTON)
                .callbackData(SHOW_LIST_COMMAND)
                .build();
            keyboard.add(List.of(button));
        }
        return inlineKeyboardMarkup;
    }

    public static InlineKeyboardMarkup makeSettingsMenu(List<ListInfoDto> allListByTelegramUserId) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        inlineKeyboardMarkup.setKeyboard(keyboard);

        allListByTelegramUserId.forEach(withCounter((i, myList) -> {
            String text = myList.isSelected() ? CHECKMARK : EMPTY;
            text += myList.getName() + " [" + myList.getOwner().getFirstName() + " " + myList.getOwner().getLastName() + "]";

            InlineKeyboardButton button = InlineKeyboardButton.builder()
                .text(text)
                .callbackData(String.format(EDIT_LIST_WITH_LIST_ID_COMMAND, myList.getId()))
                .build();
            keyboard.add(List.of(button));
        }));

        InlineKeyboardButton createButton = InlineKeyboardButton.builder()
            .text(CREATE_BUTTON)
            .callbackData(CREATE_LIST_COMMAND)
            .build();

        keyboard.add(List.of(createButton));
        return inlineKeyboardMarkup;
    }
}
