package blog.aedev.zahlebom.command.settings;

import static blog.aedev.zahlebom.command.util.MessageUtil.makeMessage;
import static blog.aedev.zahlebom.config.MainConstants.LIST_ID;
import static blog.aedev.zahlebom.config.MenuConstants.RENAME_LIST_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.RENAME_TEMPLATE;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import blog.aedev.zahlebom.listener.MessageListenerManager;
import blog.aedev.zahlebom.service.ToDoListService;
import blog.aedev.zahlebom.service.UserService;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(RENAME_LIST_COMMAND)
@Slf4j
public class RenameListCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "rename_list";

    private final TelegramBotFacade telegramBotFacade;

    public RenameListCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine, MessageListenerManager listenerManager, UserService userService, ToDoListService listService) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        Long listId = Long.parseLong(args.get(LIST_ID));
        telegramBotFacade.subscribe(listId);

        Context context = new Context();
        sender.execute(makeMessage(chat.getId(), templateEngine.process(RENAME_TEMPLATE, context)));
    }
}
