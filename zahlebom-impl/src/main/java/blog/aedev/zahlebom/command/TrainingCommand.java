package blog.aedev.zahlebom.command;

import static blog.aedev.zahlebom.command.util.MessageUtil.makeMessage;
import static blog.aedev.zahlebom.config.MenuConstants.TRAINING_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.TRAINING_TEMPLATE_1;
import static blog.aedev.zahlebom.config.ThymeleafConstants.TRAINING_TEMPLATE_2;
import static blog.aedev.zahlebom.config.ThymeleafConstants.TRAINING_TEMPLATE_3;
import static blog.aedev.zahlebom.config.ThymeleafConstants.TRAINING_TEMPLATE_4;
import static blog.aedev.zahlebom.config.ThymeleafConstants.TRAINING_TEMPLATE_5;
import static blog.aedev.zahlebom.config.ThymeleafConstants.TRAINING_TEMPLATE_6;
import static blog.aedev.zahlebom.config.ThymeleafConstants.TRAINING_TEMPLATE_7;
import static blog.aedev.zahlebom.config.ThymeleafConstants.TRAINING_TEMPLATE_8;

import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(TRAINING_COMMAND)
@Slf4j
public class TrainingCommand extends AbstractBotCommand {

    private final static String COMMAND_IDENTIFIER = "training";

    public TrainingCommand(SpringTemplateEngine templateEngine) {
        super(COMMAND_IDENTIFIER, templateEngine);
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, Map<String, String> args) throws Exception {
        sendWelcomeMessages(absSender, chat);
    }

    @Async
    public void sendWelcomeMessages(AbsSender absSender, Chat chat) throws Exception {
        Context context = new Context();
        absSender.execute(makeMessage(chat.getId(), templateEngine.process(TRAINING_TEMPLATE_1, context)));
        absSender.execute(makeMessage(chat.getId(), templateEngine.process(TRAINING_TEMPLATE_2, context)));
        Thread.sleep(5000);
        absSender.execute(makeMessage(chat.getId(), templateEngine.process(TRAINING_TEMPLATE_3, context)));
        Thread.sleep(1000);
        absSender.execute(makeMessage(chat.getId(), templateEngine.process(TRAINING_TEMPLATE_4, context)));
        Thread.sleep(1000);
        absSender.execute(makeMessage(chat.getId(), templateEngine.process(TRAINING_TEMPLATE_5, context)));
        Thread.sleep(3000);
        absSender.execute(makeMessage(chat.getId(), templateEngine.process(TRAINING_TEMPLATE_6, context)));
        absSender.execute(makeMessage(chat.getId(), templateEngine.process(TRAINING_TEMPLATE_7, context)));
        absSender.execute(makeMessage(chat.getId(), templateEngine.process(TRAINING_TEMPLATE_8, context)));
    }
}
