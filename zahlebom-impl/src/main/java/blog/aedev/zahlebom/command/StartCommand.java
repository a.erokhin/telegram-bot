package blog.aedev.zahlebom.command;

import static blog.aedev.zahlebom.command.util.MessageUtil.makeMessage;
import static blog.aedev.zahlebom.config.MenuConstants.START_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.TRAINING_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.USER_ATTRIBUTE;
import static blog.aedev.zahlebom.config.ThymeleafConstants.WELCOME_TEMPLATE_1;
import static blog.aedev.zahlebom.config.ThymeleafConstants.WELCOME_TEMPLATE_2;

import blog.aedev.zahlebom.dto.UserDto;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(START_COMMAND)
@Slf4j
public class StartCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "start";
    private static final String START = "\uD83D\uDE80 \uD83D\uDE80 \uD83D\uDE80";
    private static final String TOKEN = "token";

    private final TelegramBotFacade telegramBotFacade;

    public StartCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        UserDto userDto = telegramBotFacade.registerUser(user, args.get(TOKEN));

        Context context = new Context();
        context.setVariable(USER_ATTRIBUTE, userDto);
        sender.execute(makeMessage(chat.getId(), templateEngine.process(WELCOME_TEMPLATE_1, context)));
        sender.execute(makeMessage(chat.getId(), templateEngine.process(WELCOME_TEMPLATE_2, context), makeItemsMenu()));
    }

    private InlineKeyboardMarkup makeItemsMenu() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        inlineKeyboardMarkup.setKeyboard(keyboard);
        InlineKeyboardButton next = InlineKeyboardButton.builder()
            .text(START)
            .callbackData(TRAINING_COMMAND)
            .build();
        keyboard.add(List.of(next));
        return inlineKeyboardMarkup;
    }
}
