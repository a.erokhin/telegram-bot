package blog.aedev.zahlebom.command.settings;

import static blog.aedev.zahlebom.command.util.MenuUtil.makeSettingsMenu;
import static blog.aedev.zahlebom.command.util.MessageUtil.makeEditMessage;
import static blog.aedev.zahlebom.command.util.MessageUtil.makeMessage;
import static blog.aedev.zahlebom.config.MainConstants.MESSAGE_ID;
import static blog.aedev.zahlebom.config.MenuConstants.SETTINGS_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.SETTINGS_TEMPLATE;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.dto.ListInfoDto;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(SETTINGS_COMMAND)
@Slf4j
public class SettingsCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "settings";

    private final TelegramBotFacade telegramBotFacade;

    public SettingsCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        List<ListInfoDto> allListByTelegramUserId = telegramBotFacade.getAllListInfo();

        InlineKeyboardMarkup inlineKeyboardMarkup = makeSettingsMenu(allListByTelegramUserId);

        if (args.containsKey(MESSAGE_ID)) {
            Integer messageId = Integer.parseInt(args.get(MESSAGE_ID));
            sender.execute(makeEditMessage(chat.getId(), messageId,
                templateEngine.process(SETTINGS_TEMPLATE, new Context()), inlineKeyboardMarkup));
        } else {
            sender.execute(makeMessage(chat.getId(),
                templateEngine.process(SETTINGS_TEMPLATE, new Context()), inlineKeyboardMarkup));
        }
    }


}
