package blog.aedev.zahlebom.command.settings;

import static blog.aedev.zahlebom.config.MenuConstants.SELECT_LIST_COMMAND;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(SELECT_LIST_COMMAND)
@Slf4j
public class SelectListCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "select_list";
    private static final String LIST_ID = "list_id";

    private final TelegramBotFacade telegramBotFacade;
    private final SettingsCommand settingsCommand;

    public SelectListCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine, SettingsCommand settingsCommand) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
        this.settingsCommand = settingsCommand;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        Long listId = Long.parseLong(args.get(LIST_ID));
        telegramBotFacade.selectList(listId);

        settingsCommand.execute(sender, user, chat, args);
    }
}
