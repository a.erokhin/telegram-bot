package blog.aedev.zahlebom.command.settings;

import static blog.aedev.zahlebom.config.MenuConstants.UNSUBSCRIBE_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.USER_ATTRIBUTE;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.dto.UserDto;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.Map;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(UNSUBSCRIBE_COMMAND)
@Slf4j
public class UnsubscribeListCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "unsubscribe_list";
    private static final String LIST_ID = "list_id";
    private static final String TELEGRAM_USER_ID_KEY = "telegram_user_id";

    private final TelegramBotFacade telegramBotFacade;
    private final SettingsCommand settingsCommand;

    public UnsubscribeListCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine, SettingsCommand settingsCommand) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
        this.settingsCommand = settingsCommand;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {

        Long listId = Long.parseLong(args.get(LIST_ID));
        Long telegramUserId = Optional.ofNullable(args.get(TELEGRAM_USER_ID_KEY))
            .map(Long::parseLong)
            .orElse(user.getId());

        UserDto removedUserInfo = telegramBotFacade.unsubscribe(listId, telegramUserId);

        Context context = new Context();
        context.setVariable(USER_ATTRIBUTE, removedUserInfo);

        settingsCommand.execute(sender, user, chat, args);
    }
}
