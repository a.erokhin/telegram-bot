package blog.aedev.zahlebom.command.user;

import static blog.aedev.zahlebom.command.util.MessageUtil.makeMessage;
import static blog.aedev.zahlebom.config.MenuConstants.SHARE_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.INVITE_LINK_TEMPLATE;
import static blog.aedev.zahlebom.config.ThymeleafConstants.INVITE_URL_ATTRIBUTE;
import static blog.aedev.zahlebom.config.ThymeleafConstants.SHARE_TEMPLATE;
import static blog.aedev.zahlebom.config.ThymeleafConstants.USER_ATTRIBUTE;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.dto.InviteDto;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(SHARE_COMMAND)
@Slf4j
public class ShareCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "share";
    private static final String LIST_ID = "list_id";

    private final TelegramBotFacade telegramBotFacade;

    public ShareCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, Map<String, String> args) throws Exception {
        InviteDto invite = getInvite(args);

        Context context = new Context();
        context.setVariable(INVITE_URL_ATTRIBUTE, invite.getUrl());
        context.setVariable(USER_ATTRIBUTE, invite.getUser());

        absSender.execute(makeMessage(chat.getId(), templateEngine.process(SHARE_TEMPLATE, context)));
        absSender.execute(makeMessage(chat.getId(), templateEngine.process(INVITE_LINK_TEMPLATE, context)));
    }

    private InviteDto getInvite(Map<String, String> args) {
        if (args.containsKey(LIST_ID)) {
            Long listId = Long.parseLong(args.get(LIST_ID));
            return telegramBotFacade.invite(listId);
        }
        return telegramBotFacade.invite();
    }
}
