package blog.aedev.zahlebom.command.list;

import static blog.aedev.zahlebom.config.MenuConstants.CLEAR_LIST_COMMAND;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(CLEAR_LIST_COMMAND)
@Slf4j
public class ClearSelectedListCommand extends AbstractBotCommand {

    private final static String COMMAND_IDENTIFIER = "clear_list";

    private final TelegramBotFacade telegramBotFacade;
    private final ShowListCommand showListCommand;

    public ClearSelectedListCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine, ShowListCommand showListCommand) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
        this.showListCommand = showListCommand;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        telegramBotFacade.deletePurchasedItems();
        showListCommand.execute(sender, user, chat, args);
    }
}
