package blog.aedev.zahlebom.command.settings;

import static blog.aedev.zahlebom.command.util.MessageUtil.makeEditMessage;
import static blog.aedev.zahlebom.config.MainConstants.LIST_ID;
import static blog.aedev.zahlebom.config.MainConstants.MESSAGE_ID;
import static blog.aedev.zahlebom.config.MenuConstants.SETTINGS_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.SHOW_LIST_USERS_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.UNSUBSCRIBE_WITH_LIST_ID_AND_USER_ID_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.LIST_USERS_TEMPLATE;
import static org.apache.commons.lang3.StringUtils.SPACE;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.dto.ListInfoDto;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(SHOW_LIST_USERS_COMMAND)
@Slf4j
public class ShowListUsersCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "show_list_users";
    private static final String BACK_BUTTON = "⬅️ назад";

    private final TelegramBotFacade telegramBotFacade;

    public ShowListUsersCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        Long listId = Long.parseLong(args.get(LIST_ID));
        ListInfoDto list = telegramBotFacade.getListInfoById(listId);

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        inlineKeyboardMarkup.setKeyboard(keyboard);

        list.getUsers().forEach(u -> {
            InlineKeyboardButton button = InlineKeyboardButton.builder()
                .text(u.getFirstName() + SPACE + u.getLastName())
                .callbackData(String.format(UNSUBSCRIBE_WITH_LIST_ID_AND_USER_ID_COMMAND, list.getId(), u.getId()))
                .build();
            keyboard.add(List.of(button));
        });

        InlineKeyboardButton backButton = InlineKeyboardButton.builder()
            .text(BACK_BUTTON)
            .callbackData(SETTINGS_COMMAND)
            .build();
        keyboard.add(List.of(backButton));

        Integer messageId = Integer.parseInt(args.get(MESSAGE_ID));
        sender.execute(makeEditMessage(chat.getId(), messageId,
            templateEngine.process(LIST_USERS_TEMPLATE, new Context()), inlineKeyboardMarkup));
    }
}
