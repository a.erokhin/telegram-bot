package blog.aedev.zahlebom.command.list;

import static blog.aedev.zahlebom.command.util.MessageUtil.makeEditMessage;
import static blog.aedev.zahlebom.command.util.MessageUtil.makeMessage;
import static blog.aedev.zahlebom.config.MainConstants.MESSAGE_ID;
import static blog.aedev.zahlebom.config.MenuConstants.CLEAN_LIST_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.CLEAR_LIST_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.SHOW_EDITABLE_LIST;
import static blog.aedev.zahlebom.config.MenuConstants.SHOW_ORDERED_LIST;
import static blog.aedev.zahlebom.config.MenuConstants.SHOW_SELECTABLE_LIST;
import static blog.aedev.zahlebom.config.ThymeleafConstants.ITEMS_ATTRIBUTE;
import static blog.aedev.zahlebom.config.ThymeleafConstants.ORDERED_LIST_TEMPLATE;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.dto.ItemDto;
import blog.aedev.zahlebom.dto.ListDto;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(SHOW_ORDERED_LIST)
@Slf4j
public class ShowOrderedListCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "show_ordered_list";
    private static final String MAKE_SELECTIVE = "✅ Отметить";
    private static final String EDIT = "✏️ Вычеркнуть";
    private static final String CLEAN = "\uD83E\uDDFD Очистить";
    private static final String CLEAR_SELECTED = "❌ Удалить отмеченные";

    private final TelegramBotFacade telegramBotFacade;

    public ShowOrderedListCommand(SpringTemplateEngine templateEngine, TelegramBotFacade telegramBotFacade) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        ListDto list = telegramBotFacade.getCurrentList();

        InlineKeyboardMarkup inlineKeyboardMarkup = list.getItems().size() > 0 ? makeInlineKeyboardMarkup(list.getItems()) : null;

        Context context = new Context();
        context.setVariable(ITEMS_ATTRIBUTE, list.getItems());

        if (args.containsKey(MESSAGE_ID)) {
            Integer messageId = Integer.parseInt(args.get(MESSAGE_ID));
            sender.execute(makeEditMessage(chat.getId(), messageId,
                templateEngine.process(ORDERED_LIST_TEMPLATE, context),
                inlineKeyboardMarkup));
        } else {
            sender.execute(makeMessage(chat.getId(),
                templateEngine.process(ORDERED_LIST_TEMPLATE, context), inlineKeyboardMarkup));
        }
    }

    private InlineKeyboardMarkup makeInlineKeyboardMarkup(List<ItemDto> items) {
        InlineKeyboardMarkup inlineKeyboardMarkup;
        inlineKeyboardMarkup = new InlineKeyboardMarkup();
        InlineKeyboardButton makeSelectiveButton = InlineKeyboardButton.builder()
            .text(MAKE_SELECTIVE)
            .callbackData(SHOW_SELECTABLE_LIST)
            .build();
        InlineKeyboardButton editButton = InlineKeyboardButton.builder()
            .text(EDIT)
            .callbackData(SHOW_EDITABLE_LIST)
            .build();

        List<InlineKeyboardButton> lastMenu = new ArrayList<>();
        if (items.stream()
            .anyMatch(ItemDto::getIsPurchased)) {
                InlineKeyboardButton clearSelectedButton = InlineKeyboardButton.builder()
                    .text(CLEAR_SELECTED)
                    .callbackData(CLEAR_LIST_COMMAND)
                    .build();
                lastMenu.add(clearSelectedButton);
        }

        InlineKeyboardButton cleanButton = InlineKeyboardButton.builder()
            .text(CLEAN)
            .callbackData(CLEAN_LIST_COMMAND)
            .build();
        lastMenu.add(cleanButton);

        inlineKeyboardMarkup.setKeyboard(List.of(List.of(makeSelectiveButton, editButton), lastMenu));
        return inlineKeyboardMarkup;
    }
}
