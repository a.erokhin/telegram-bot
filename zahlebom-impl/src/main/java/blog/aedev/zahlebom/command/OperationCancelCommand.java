package blog.aedev.zahlebom.command;

import static blog.aedev.zahlebom.command.util.MenuUtil.makeItemsMenu;
import static blog.aedev.zahlebom.command.util.MessageUtil.makeEditMessage;
import static blog.aedev.zahlebom.config.MainConstants.LIST_ID;
import static blog.aedev.zahlebom.config.MainConstants.MESSAGE_ID;
import static blog.aedev.zahlebom.config.MenuConstants.OPERATION_CANCEL_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.REMOVE_ITEM_WITH_ITEM_ID_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.EDITABLE_LIST_TEMPLATE;

import blog.aedev.zahlebom.dto.ItemDto;
import blog.aedev.zahlebom.dto.ListDto;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(OPERATION_CANCEL_COMMAND)
@Slf4j
public class OperationCancelCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "operation_cancel";

    private final TelegramBotFacade telegramBotFacade;

    public OperationCancelCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        Long listId = Long.parseLong(args.get(LIST_ID));
        telegramBotFacade.lastOperationCancel(listId);
        ListDto list = telegramBotFacade.getCurrentList();

        Integer messageId = Integer.parseInt(args.get(MESSAGE_ID));
        sender.execute(makeEditMessage(chat.getId(), messageId,
            templateEngine.process(EDITABLE_LIST_TEMPLATE, new Context()),
            makeItemsMenu(list, REMOVE_ITEM_WITH_ITEM_ID_COMMAND)));
    }
}
