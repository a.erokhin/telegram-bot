package blog.aedev.zahlebom.command;

import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

public interface CommandExecuting {
    void execute(AbsSender sender, User user, Chat chat, String[] args);
}
