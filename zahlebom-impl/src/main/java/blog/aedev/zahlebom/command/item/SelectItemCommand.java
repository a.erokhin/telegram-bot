package blog.aedev.zahlebom.command.item;

import static blog.aedev.zahlebom.command.util.MenuUtil.makeItemsMenu;
import static blog.aedev.zahlebom.command.util.MessageUtil.makeEditMessage;
import static blog.aedev.zahlebom.config.MainConstants.ITEM_ID;
import static blog.aedev.zahlebom.config.MainConstants.MESSAGE_ID;
import static blog.aedev.zahlebom.config.MenuConstants.SELECT_ITEM_COMMAND;
import static blog.aedev.zahlebom.config.MenuConstants.SELECT_ITEM_WITH_ITEM_ID_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.SELECTABLE_LIST_TEMPLATE;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.dto.ItemDto;
import blog.aedev.zahlebom.dto.ListDto;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(SELECT_ITEM_COMMAND)
@Slf4j
public class SelectItemCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "show_selectable_list";

    private final TelegramBotFacade telegramBotFacade;

    public SelectItemCommand(SpringTemplateEngine templateEngine, TelegramBotFacade telegramBotFacade) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        Long itemId = Long.parseLong(args.get(ITEM_ID));
        ListDto list = telegramBotFacade.markItemAsPurchasedAndGetList(itemId);

        Integer messageId = Integer.parseInt(args.get(MESSAGE_ID));
        sender.execute(makeEditMessage(chat.getId(), messageId,
            templateEngine.process(SELECTABLE_LIST_TEMPLATE, new Context()),
            makeItemsMenu(list, SELECT_ITEM_WITH_ITEM_ID_COMMAND)));
    }
}

