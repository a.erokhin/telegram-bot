package blog.aedev.zahlebom.command.settings;

import static blog.aedev.zahlebom.command.util.MenuUtil.makeEditListMenu;
import static blog.aedev.zahlebom.command.util.MessageUtil.makeEditMessage;
import static blog.aedev.zahlebom.config.MainConstants.MESSAGE_ID;
import static blog.aedev.zahlebom.config.MenuConstants.CREATE_LIST_COMMAND;
import static blog.aedev.zahlebom.config.ThymeleafConstants.CREATE_LIST_TEMPLATE;
import static blog.aedev.zahlebom.config.ThymeleafConstants.LIST_ATTRIBUTE;

import blog.aedev.zahlebom.command.AbstractBotCommand;
import blog.aedev.zahlebom.dto.ListInfoDto;
import blog.aedev.zahlebom.facade.TelegramBotFacade;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Component(CREATE_LIST_COMMAND)
@Slf4j
public class CreateListCommand extends AbstractBotCommand {

    private static final String COMMAND_IDENTIFIER = "create_list";

    private final TelegramBotFacade telegramBotFacade;

    public CreateListCommand(TelegramBotFacade telegramBotFacade, SpringTemplateEngine templateEngine) {
        super(COMMAND_IDENTIFIER, templateEngine);
        this.telegramBotFacade = telegramBotFacade;
    }

    @Override
    public void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception {
        ListInfoDto list = telegramBotFacade.createList();
        Context context = new Context();
        context.setVariable(LIST_ATTRIBUTE, list);

        Integer messageId = Integer.parseInt(args.get(MESSAGE_ID));
        sender.execute(makeEditMessage(chat.getId(), messageId,
            templateEngine.process(CREATE_LIST_TEMPLATE, context), makeEditListMenu(list)));
    }
}
