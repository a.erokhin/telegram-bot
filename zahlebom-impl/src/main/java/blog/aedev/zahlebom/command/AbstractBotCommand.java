package blog.aedev.zahlebom.command;

import static org.apache.commons.lang3.StringUtils.EMPTY;

import blog.aedev.zahlebom.exception.ExceptionHandler;
import blog.aedev.zahlebom.repository.ThreadLocalStorage;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.thymeleaf.spring5.SpringTemplateEngine;

@Slf4j
public abstract class AbstractBotCommand extends BotCommand implements CommandExecuting {

    private static final String KEY_SEPARATOR = "=";

    protected final SpringTemplateEngine templateEngine;

    public AbstractBotCommand(String commandIdentifier, SpringTemplateEngine templateEngine) {
        super(commandIdentifier, EMPTY);
        this.templateEngine = templateEngine;
    }

    public void execute(AbsSender sender, User user, Chat chat, String[] args) {
        ThreadLocalStorage.setUserId(user.getId());
        Map<String, String> map = argsToMap(args);
        try {
            execute(sender, user, chat, map);
        } catch (Exception e) {
            ExceptionHandler.handle(e, sender, chat.getId());
        } finally {
            ThreadLocalStorage.setUserId(null);
        }
    }

    public abstract void execute(AbsSender sender, User user, Chat chat, Map<String, String> args) throws Exception;

    private Map<String, String> argsToMap(String[] args) {
        if (args == null) return Map.of();
        return Arrays.stream(args)
            .map(a -> {
                String[] split = a.split(KEY_SEPARATOR);
                return split.length == 2 ? new AbstractMap.SimpleEntry<>(split[0], split[1]) : null;
            })
            .filter(Objects::nonNull)
            .collect(Collectors.toUnmodifiableMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue));
    }
}
