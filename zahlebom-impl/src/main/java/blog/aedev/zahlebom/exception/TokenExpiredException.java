package blog.aedev.zahlebom.exception;

public class TokenExpiredException extends BusinessException {

    private static final String MESSAGE = "\uD83E\uDD6B Приглашение истекло, " +
        "попроси своего друга отправить тебе новое.";

    public TokenExpiredException() {
        super(MESSAGE);
    }

    public TokenExpiredException(String message) {
        super(message);
    }
}
