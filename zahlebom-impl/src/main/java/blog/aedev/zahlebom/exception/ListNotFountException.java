package blog.aedev.zahlebom.exception;

public class ListNotFountException extends BusinessException {

    private static final String ERROR_MESSAGE = "\uD83E\uDD6B Список не найден, возможно он был удален.";

    public ListNotFountException() {
        super(ERROR_MESSAGE);
    }

    public ListNotFountException(String message) {
        super(message);
    }
}
