package blog.aedev.zahlebom.exception;

public class UserNotFoundException extends BusinessException {

    private static final String MESSAGE = "Я не нашел тебя в своей базе, помоги мне нажми \uD83D\uDC49 /start";

    public UserNotFoundException() {
        super(MESSAGE);
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}
