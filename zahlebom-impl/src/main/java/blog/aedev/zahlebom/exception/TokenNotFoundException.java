package blog.aedev.zahlebom.exception;

public class TokenNotFoundException extends BusinessException {

    private static final String MESSAGE = "У меня заминка, " +
        "я не нашел приглашение к общему списку, " +
        "попроси друга отправить тебе ссылку еще раз или нажми \uD83D\uDC49 /start, " +
        "чтобы создать свой список";

    public TokenNotFoundException() {
        super(MESSAGE);
    }

    public TokenNotFoundException(String message) {
        super(message);
    }
}
