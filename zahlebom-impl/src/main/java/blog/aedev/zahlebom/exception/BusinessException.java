package blog.aedev.zahlebom.exception;

import static blog.aedev.zahlebom.exception.Constants.DEFAULT_ERR_MESSAGE;

public abstract class BusinessException extends RuntimeException {

    public BusinessException() {
        super(DEFAULT_ERR_MESSAGE);
    }

    public BusinessException(String message) {
        super(message);
    }
}

