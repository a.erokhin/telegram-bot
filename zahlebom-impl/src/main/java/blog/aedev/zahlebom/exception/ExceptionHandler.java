package blog.aedev.zahlebom.exception;

import static blog.aedev.zahlebom.command.util.MessageUtil.makeMessage;
import static blog.aedev.zahlebom.exception.Constants.DEFAULT_ERR_MESSAGE;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Slf4j
public class ExceptionHandler {

    @SneakyThrows
    public static void handle(Exception e, AbsSender sender, Long chatId) {
        log.error(e.getMessage(), e);
        if (e instanceof BusinessException) {
            sender.execute(makeMessage(chatId, e.getMessage()));
        }  else {
            sender.execute(makeMessage(chatId, DEFAULT_ERR_MESSAGE));
        }
    }
}
