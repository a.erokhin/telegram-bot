package blog.aedev.zahlebom.exception;

public class OperationException extends BusinessException {

    public OperationException(String message) {
        super(message);
    }
}
