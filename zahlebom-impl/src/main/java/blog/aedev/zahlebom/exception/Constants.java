package blog.aedev.zahlebom.exception;

public interface Constants {

    String DEFAULT_ERR_MESSAGE = "\uD83E\uDD14 Кажется, у меня возникла проблема. Попробуй еще раз.";
}
