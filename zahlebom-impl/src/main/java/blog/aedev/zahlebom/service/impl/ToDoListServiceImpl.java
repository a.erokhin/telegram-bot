package blog.aedev.zahlebom.service.impl;

import blog.aedev.zahlebom.exception.ListNotFountException;
import blog.aedev.zahlebom.exception.OperationException;
import blog.aedev.zahlebom.listener.MessageListenerManager;
import blog.aedev.zahlebom.model.ToDoList;
import blog.aedev.zahlebom.model.UserInfo;
import blog.aedev.zahlebom.repository.ThreadLocalStorage;
import blog.aedev.zahlebom.repository.ToDoListRepository;
import blog.aedev.zahlebom.service.ItemService;
import blog.aedev.zahlebom.service.ToDoListService;
import blog.aedev.zahlebom.service.UserService;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ToDoListServiceImpl implements ToDoListService {

    private static final String DEFAULT_NAME = "Default";
    private static final String DD_MM_YYYY = "dd-MM-yyyy";
    private static final String DELETE_OWNER_ERROR_MESSAGE = "\uD83E\uDD6B Погоди, нельзя удалить владельца списка.";

    private final ToDoListRepository listRepository;
    private final ItemService itemService;
    private final UserService userService;
    private final MessageListenerManager listenerManager;

    @Override
    public ToDoList create() {
        UserInfo user = userService.getCurrentUser();

        return listRepository.save(ToDoList.builder()
            .name(generateName())
            .owner(user)
            .users(Set.of(user))
            .build());
    }

    @Override
    @Transactional(readOnly = true)
    public Long getIdOfCurrentList() {
        return listRepository.findIdOfCurrentList(ThreadLocalStorage.getUserId())
            .orElseThrow(ListNotFountException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public ToDoList getOfCurrentList() {
        return listRepository.getById(ThreadLocalStorage.getUserId());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ToDoList> getAllByCurrentUser() {
        UserInfo user = userService.get(ThreadLocalStorage.getUserId());
        return getByUser(user);
    }

    @Override
    @Transactional(readOnly = true)
    public ToDoList getByIdAndCurrentUser(Long id) {
        return getByIdAndUser(id, userService.getCurrentUser());
    }

    @Override
    public void delete(Long id) {
        UserInfo user = userService.getCurrentUser();
        ToDoList list = getByIdAndUser(id, user);

        listRepository.save(list.toBuilder()
            .deleted(true)
            .build());

        // Если удаляемый лист является текущим листом пользователя,
        // то берем любой лист пользователя и устанавливаем в текущий.
        // Если у пользователя не осталось листов, присваиваем null.
        if (user.getSelectList().getId().equals(list.getId())) {
            getByUser(user).stream()
                .findFirst()
                .ifPresentOrElse(l -> userService.setList(l.getId()), () -> userService.setList(null));
        }
    }

    @Override
    public void addUser(ToDoList list) {
        list.getUsers().add(userService.getCurrentUser());
        listRepository.save(list);
    }

    @Override
    public UserInfo removeUser(Long id, Long userId) {
        ToDoList list = listRepository.getById(id);
        UserInfo user = userService.get(userId);

        if (list.getOwner().equals(user)) {
            throw new OperationException(DELETE_OWNER_ERROR_MESSAGE);
        }

        list.getUsers().remove(user);
        listRepository.save(list);

        // Если удаляемый лист является текущим листом пользователя,
        // то берем любой лист пользователя и устанавливаем в текущий.
        // Если у пользователя не осталось листов, присваиваем null.
        if (user.getSelectList().getId().equals(id)) {
            getByUser(user).stream()
                .findFirst()
                .ifPresentOrElse(l -> userService.setList(l.getId()), () -> userService.setList(null));
        }

        return user;
    }

    @Override
    public void rename(String newName, Long listId) {
        ToDoList list = listRepository.getById(listId);
        listRepository.save(list.toBuilder()
            .name(newName)
            .build());
        listenerManager.unSubscribe();
    }

    @Override
    public void clean() {
        Long id = getIdOfCurrentList();
        itemService.deleteAll(id);
    }

    private String generateName() {
        return DEFAULT_NAME + " [" + LocalDate.now().format(DateTimeFormatter.ofPattern(DD_MM_YYYY)) + "]";
    }

    private List<ToDoList> getByUser(UserInfo user) {
        return listRepository.findAllByUsersContainsAndDeletedIsFalse(user);
    }

    private ToDoList getByIdAndUser(Long id, UserInfo userInfo) {
        ToDoList list = listRepository.findByIdAndUsersContainsAndDeletedIsFalse(id, userInfo)
            .orElseThrow(ListNotFountException::new);
        list.setSelectForCurrentUser(userInfo.getSelectList().getId().equals(list.getId()));
        return list;
    }
}
