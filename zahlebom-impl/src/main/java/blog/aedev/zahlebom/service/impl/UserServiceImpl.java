package blog.aedev.zahlebom.service.impl;

import blog.aedev.zahlebom.exception.UserNotFoundException;
import blog.aedev.zahlebom.model.Audit;
import blog.aedev.zahlebom.model.UserInfo;
import blog.aedev.zahlebom.repository.ThreadLocalStorage;
import blog.aedev.zahlebom.repository.UserRepository;
import blog.aedev.zahlebom.service.ToDoListService;
import blog.aedev.zahlebom.service.TokenService;
import blog.aedev.zahlebom.service.UserService;
import java.time.OffsetDateTime;
import javax.persistence.EntityManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ToDoListService listService;
    private final TokenService tokenService;
    private final EntityManager entityManager;

    public UserServiceImpl(UserRepository userRepository,
                           @Lazy ToDoListService listService,
                           TokenService tokenService,
                           EntityManager entityManager) {
        this.userRepository = userRepository;
        this.listService = listService;
        this.tokenService = tokenService;
        this.entityManager = entityManager;
    }

    @Override
    public UserInfo getOrCreate(UserInfo userInfo, String tokenValue) {
        UserInfo savedUser = userRepository.findById(userInfo.getId()).orElseGet(() -> {
            fillAudit(userInfo);
            return userRepository.save(userInfo).toBuilder()
                .isNew(true)
                .build();
        });

        if (StringUtils.isNotBlank(tokenValue)) {
            var token = tokenService.validateAndGet(tokenValue);
            listService.addUser(token.getList());
            savedUser.setSelectList(token.getList());
        } else if (savedUser.isNew()) {
            var list = listService.create();
            savedUser.setSelectList(list);
        }

        return userRepository.save(savedUser).toBuilder()
            .isNew(savedUser.isNew())
            .build();
    }

    @Override
    @Transactional(readOnly = true)
    public UserInfo get(Long id) {
        return userRepository.findById(id)
            .orElseThrow(UserNotFoundException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public UserInfo getCurrentUser() {
        return userRepository.findById(ThreadLocalStorage.getUserId())
            .orElseThrow(UserNotFoundException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public UserInfo getCurrentUserWithList() {
        entityManager.flush();
        return userRepository.findOneByIdWithEntityGraph(ThreadLocalStorage.getUserId())
            .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public UserInfo setList(Long listId) {
        UserInfo user = getCurrentUser().toBuilder()
            .selectList(listId == null ? null : listService.getByIdAndCurrentUser(listId))
            .build();
        return userRepository.save(user);
    }

    private void fillAudit(UserInfo userInfo) {
        userInfo.setAudit(Audit.builder()
            .createdBy(ThreadLocalStorage.getUserId())
            .createdDate(OffsetDateTime.now())
            .lastModifiedBy(ThreadLocalStorage.getUserId())
            .lastModifiedDate(OffsetDateTime.now())
            .build());
    }
}
