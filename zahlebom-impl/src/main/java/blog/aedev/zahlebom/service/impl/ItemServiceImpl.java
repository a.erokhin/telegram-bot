package blog.aedev.zahlebom.service.impl;

import blog.aedev.zahlebom.mapper.ItemMapper;
import blog.aedev.zahlebom.model.DeleteItem;
import blog.aedev.zahlebom.model.Item;
import blog.aedev.zahlebom.model.Product;
import blog.aedev.zahlebom.repository.DeleteItemRepository;
import blog.aedev.zahlebom.repository.ItemRepository;
import blog.aedev.zahlebom.repository.ProductRepository;
import blog.aedev.zahlebom.service.ItemService;
import blog.aedev.zahlebom.service.ToDoListService;
import blog.aedev.zahlebom.service.UserService;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;
    private final ProductRepository productRepository;
    private final UserService userService;
    private final ItemMapper itemMapper;
    private final ToDoListService listService;
    private final DeleteItemRepository deleteItemRepository;

    public ItemServiceImpl(ItemRepository itemRepository,
                           ProductRepository productRepository,
                           UserService userService,
                           ItemMapper itemMapper,
                           @Lazy ToDoListService listService,
                           DeleteItemRepository deleteItemRepository) {
        this.itemRepository = itemRepository;
        this.productRepository = productRepository;
        this.userService = userService;
        this.itemMapper = itemMapper;
        this.listService = listService;
        this.deleteItemRepository = deleteItemRepository;
    }

    @Override
    public Item create(String message) {
        Product product = productRepository.findByName(message.trim())
            .orElseGet(() -> productRepository.save(Product.builder()
                .name(message.trim())
                .build())
            );

        Item item = Item.builder()
            .product(product)
            .list(userService.getCurrentUser().getSelectList())
            .purchased(false)
            .build();

        return itemRepository.save(item);
    }

    @Override
    public List<Item> create(List<String> messages) {
        return messages.stream()
            .map(this::create)
            .collect(Collectors.toList());
    }

    @Override
    public void markAsPurchased(Long itemId) {
        Item item = itemRepository.getById(itemId);

        itemRepository.save(item.toBuilder()
            .purchased(!item.isPurchased())
            .build());
    }

    @Override
    public void deleteAll(Long listId) {
        List<Item> items = itemRepository.findAllByListId(listId);
        deleteAll(items);
    }

    @Override
    public void deletePurchased() {
        List<Item> items = itemRepository.findAllByListIdAndPurchased(listService.getIdOfCurrentList(), true);
        deleteAll(items);
    }

    @Override
    public void restore(Long listId) {
        Optional<DeleteItem> page = deleteItemRepository.findByListIdAndLastModified(listId, PageRequest.of(0, 1)).stream()
            .findFirst();

        if (page.isPresent()) {
            Item item = itemMapper.map(page.get());
            itemRepository.save(item);
            deleteItemRepository.delete(page.get());
        }
    }

    @Override
    public void delete(Long itemId) {
        Item item = itemRepository.findById(itemId).orElseThrow();
        DeleteItem deleteItem = itemMapper.mapToDeleteItem(item);
        itemRepository.delete(item);
        deleteItemRepository.save(deleteItem);
    }

    private void deleteAll(List<Item> items) {
        List<DeleteItem> deleteItems = itemMapper.mapToDeleteItem(items);
        deleteItemRepository.saveAll(deleteItems);
        itemRepository.deleteAll(items);
    }
}
