package blog.aedev.zahlebom.service;

import blog.aedev.zahlebom.model.ToDoList;
import blog.aedev.zahlebom.model.Token;
import blog.aedev.zahlebom.model.UserInfo;

public interface TokenService {
    Token create(ToDoList list);

    Token validateAndGet(String value);
}
