package blog.aedev.zahlebom.service;

import blog.aedev.zahlebom.model.ToDoList;
import blog.aedev.zahlebom.model.UserInfo;
import java.util.List;

public interface ToDoListService {
    ToDoList create();

    Long getIdOfCurrentList();

    ToDoList getOfCurrentList();

    ToDoList getByIdAndCurrentUser(Long id);

    List<ToDoList> getAllByCurrentUser();

    void delete(Long id);

    void addUser(ToDoList list);

    UserInfo removeUser(Long id, Long userId);

    void rename(String newName, Long listId);

    void clean();
}
