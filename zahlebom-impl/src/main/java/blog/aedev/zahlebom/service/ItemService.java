package blog.aedev.zahlebom.service;

import blog.aedev.zahlebom.model.Item;
import blog.aedev.zahlebom.model.UserInfo;
import java.util.List;

public interface ItemService {
    Item create(String message);

    List<Item> create(List<String> messages);

    void markAsPurchased(Long itemId);

    void delete(Long itemId);

    void deleteAll(Long listId);

    void deletePurchased();

    void restore(Long listId);
}
