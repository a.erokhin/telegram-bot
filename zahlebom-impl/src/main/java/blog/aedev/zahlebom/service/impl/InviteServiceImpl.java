package blog.aedev.zahlebom.service.impl;

import blog.aedev.zahlebom.config.ApplicationProperties;
import blog.aedev.zahlebom.dto.InviteDto;
import blog.aedev.zahlebom.mapper.UserMapper;
import blog.aedev.zahlebom.model.ToDoList;
import blog.aedev.zahlebom.model.Token;
import blog.aedev.zahlebom.model.UserInfo;
import blog.aedev.zahlebom.repository.ThreadLocalStorage;
import blog.aedev.zahlebom.service.InviteService;
import blog.aedev.zahlebom.service.ToDoListService;
import blog.aedev.zahlebom.service.TokenService;
import blog.aedev.zahlebom.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class InviteServiceImpl implements InviteService {

    private static final String INVITE_LINK = "https://t.me/%s?start=token=%s";

    private final ApplicationProperties applicationProperties;
    private final TokenService tokenService;
    private final UserService userService;
    private final ToDoListService listService;
    private final UserMapper userMapper;

    @Override
    public InviteDto invite() {
        return invite(listService.getIdOfCurrentList());
    }

    @Override
    public InviteDto invite(Long listId) {
        UserInfo user = userService.get(ThreadLocalStorage.getUserId());
        ToDoList list = listService.getByIdAndCurrentUser(listId);
        Token token = tokenService.create(list);

        return InviteDto.builder()
            .url(String.format(INVITE_LINK, applicationProperties.getTelegramBot().getUsername(), token.getValue()))
            .user(userMapper.map(user))
            .build();
    }
}
