package blog.aedev.zahlebom.service;

import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.bots.AbsSender;

public interface MessageService {
    void execute(Update update, AbsSender sender);
}
