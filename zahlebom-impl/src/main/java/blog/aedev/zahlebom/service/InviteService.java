package blog.aedev.zahlebom.service;

import blog.aedev.zahlebom.dto.InviteDto;

public interface InviteService {
    InviteDto invite();

    InviteDto invite(Long listId);
}
