package blog.aedev.zahlebom.service.impl;

import blog.aedev.zahlebom.config.ApplicationProperties;
import blog.aedev.zahlebom.exception.TokenExpiredException;
import blog.aedev.zahlebom.exception.TokenNotFoundException;
import blog.aedev.zahlebom.model.ToDoList;
import blog.aedev.zahlebom.model.Token;
import blog.aedev.zahlebom.repository.TokenRepository;
import blog.aedev.zahlebom.service.TokenService;
import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class TokenServiceImpl implements TokenService {

    private final TokenRepository tokenRepository;
    private final ApplicationProperties applicationProperties;

    @Override
    public Token create(ToDoList list) {
        Token token = Token.builder()
            .expirationDate(LocalDateTime.now().plus(applicationProperties.getToken().getExpiration()))
            .value(RandomStringUtils.randomAlphanumeric(applicationProperties.getToken().getLength()))
            .list(list)
            .build();
        return tokenRepository.save(token);
    }

    @Override
    @Transactional(readOnly = true)
    public Token validateAndGet(String value) {
        Token token = tokenRepository.findByValue(value)
            .orElseThrow(TokenNotFoundException::new);

        if (token.getExpirationDate().isBefore(LocalDateTime.now())) {
            throw new TokenExpiredException();
        }

        return token;
    }
}
