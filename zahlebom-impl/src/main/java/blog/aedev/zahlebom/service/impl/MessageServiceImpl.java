package blog.aedev.zahlebom.service.impl;

import blog.aedev.zahlebom.command.CommandExecuting;
import blog.aedev.zahlebom.listener.MessageListenerManager;
import blog.aedev.zahlebom.repository.ThreadLocalStorage;
import blog.aedev.zahlebom.service.ItemService;
import blog.aedev.zahlebom.service.MessageService;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.bots.AbsSender;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {

    private static final String SEPARATOR = "?";
    private static final String PARAM_SEPARATOR = "&";

    private final Map<String, CommandExecuting> commandMap;
    private final MessageListenerManager messageListenerManager;
    private final ItemService itemService;

    @Override
    public void execute(Update update, AbsSender sender) {
        if (update.hasCallbackQuery()) {
            executeCommand(update, sender);
        } else if (update.hasMessage()) {
            ThreadLocalStorage.setUserId(update.getMessage().getFrom().getId());

            User telegramUser = update.getMessage().getFrom();
            Chat chat = update.getMessage().getChat();
            String message = update.getMessage().getText().trim();

            if (messageListenerManager.notify(update, sender)) {
                ThreadLocalStorage.setUserId(null);
                return;
            }

            CommandExecuting botCommand = commandMap.get(message);
            if (botCommand != null) {
                botCommand.execute(sender, telegramUser, chat, null);
            } else {
                createItem(message);
                ThreadLocalStorage.setUserId(null);
            }
        }
    }

    private void executeCommand(Update update, AbsSender sender) {
        String command = getCommand(update.getCallbackQuery());
        List<String> parameters = parseParameters(update.getCallbackQuery());

        CommandExecuting botCommand = commandMap.get(command);
        botCommand.execute(sender, update.getCallbackQuery().getFrom(), update.getCallbackQuery().getMessage().getChat(), parameters.toArray(new String[0]));
    }

    private void createItem(String message) {
        itemService.create(Arrays.asList(message.split("\n")));
    }

    private String getCommand(CallbackQuery callbackQuery) {
        String data = callbackQuery.getData();
        int indexOfSeparator = data.indexOf(SEPARATOR);
        return indexOfSeparator > -1 ? data.substring(0, indexOfSeparator) : data;
    }

    private List<String> parseParameters(CallbackQuery callbackQuery) {
        String data = callbackQuery.getData();
        int indexOfSeparator = data.indexOf(SEPARATOR);

        String argsString = indexOfSeparator > -1 ? data.substring(indexOfSeparator + 1) : "";

        List<String> parameters = Arrays.stream(argsString.split(PARAM_SEPARATOR))
            .collect(Collectors.toList());

        parameters.add("message_id=" + callbackQuery.getMessage().getMessageId().toString());
        return parameters;
    }
}
