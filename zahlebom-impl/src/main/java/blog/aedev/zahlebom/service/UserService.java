package blog.aedev.zahlebom.service;

import blog.aedev.zahlebom.model.UserInfo;

public interface UserService {
    UserInfo getOrCreate(UserInfo userInfo, String tokenValue);

    UserInfo get(Long id);

    UserInfo getCurrentUser();

    UserInfo getCurrentUserWithList();

    UserInfo setList(Long listId);
}
