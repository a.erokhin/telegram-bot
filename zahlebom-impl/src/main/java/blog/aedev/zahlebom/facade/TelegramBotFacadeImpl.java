package blog.aedev.zahlebom.facade;

import blog.aedev.zahlebom.dto.InviteDto;
import blog.aedev.zahlebom.dto.ListDto;
import blog.aedev.zahlebom.dto.ListInfoDto;
import blog.aedev.zahlebom.dto.UserDto;
import blog.aedev.zahlebom.listener.MessageListener;
import blog.aedev.zahlebom.listener.MessageListenerManager;
import blog.aedev.zahlebom.listener.impl.RenameListListener;
import blog.aedev.zahlebom.mapper.ToDoListMapper;
import blog.aedev.zahlebom.mapper.UserMapper;
import blog.aedev.zahlebom.model.ToDoList;
import blog.aedev.zahlebom.model.UserInfo;
import blog.aedev.zahlebom.service.InviteService;
import blog.aedev.zahlebom.service.ItemService;
import blog.aedev.zahlebom.service.ToDoListService;
import blog.aedev.zahlebom.service.UserService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.objects.User;

@Service
@RequiredArgsConstructor
public class TelegramBotFacadeImpl implements TelegramBotFacade {

    private static final String MILK = "Молоко";
    private static final String EGGS = "Яйца";
    private static final String FLOUR = "Мука";

    private final UserService userService;
    private final ItemService itemService;
    private final ToDoListService listService;
    private final InviteService inviteService;

    private final UserMapper userMapper;
    private final ToDoListMapper listMapper;

    private final MessageListenerManager listenerManager;

    @Override
    @Transactional
    public UserDto registerUser(User telegramUser, String tokenValue) {
        UserInfo userInfo = userService.getOrCreate(userMapper.map(telegramUser), tokenValue);

        if (userInfo.isNew() && StringUtils.isBlank(tokenValue)) {
            itemService.create(MILK);
            itemService.create(EGGS);
            itemService.create(FLOUR);
        }

        return userMapper.map(userInfo);
    }

    @Override
    @Transactional
    public ListInfoDto createList() {
        ToDoList list = listService.create();
        return listMapper.mapToListInfoDto(list);
    }

    @Override
    public ListDto getCurrentList() {
        UserInfo user = userService.getCurrentUserWithList();
        return listMapper.mapToListDto(user.getSelectList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ListInfoDto> getAllListInfo() {
        List<ToDoList> lists = listService.getAllByCurrentUser();

        return listMapper.mapToListInfoDto(lists);
    }

    @Override
    @Transactional(readOnly = true)
    public ListInfoDto getListInfoById(Long listId) {
        ToDoList list = listService.getByIdAndCurrentUser(listId);
        return listMapper.mapToListInfoDto(list);
    }

    @Override
    @Transactional
    public void cleanList() {
        listService.clean();
    }

    @Override
    @Transactional
    public void deleteList(Long listId) {
        listService.delete(listId);
    }

    @Override
    @Transactional
    public void deletePurchasedItems() {
        itemService.deletePurchased();
    }

    @Override
    @Transactional
    public InviteDto invite() {
        return inviteService.invite();
    }

    @Override
    @Transactional
    public InviteDto invite(Long listId) {
        return inviteService.invite(listId);
    }

    @Override
    @Transactional
    public UserDto unsubscribe(Long listId, Long removedUserId) {
        UserInfo user = listService.removeUser(listId, removedUserId);
        return userMapper.map(user);
    }

    @Override
    public void selectList(Long listId) {
        userService.setList(listId);
    }

    @Override
    @Transactional
    public ListDto markItemAsPurchasedAndGetList(Long itemId) {
        itemService.markAsPurchased(itemId);
        return getCurrentList();
    }

    @Override
    @Transactional
    public ListDto deleteItem(Long itemId) {
        itemService.delete(itemId);
        return getCurrentList();
    }

    @Override
    @Transactional
    public void subscribe(Long listId) {
        ToDoList list = listService.getByIdAndCurrentUser(listId);
        MessageListener listener = getRenameListListener();
        listener.setObjectId(list.getId());
        listenerManager.subscribe(listener);
    }

    @Override
    @Transactional
    public ListDto lastOperationCancel(Long listId) {
        itemService.restore(listId);
        return getCurrentList();
    }

    @Lookup
    public RenameListListener getRenameListListener() {
        return null;
    }
}
