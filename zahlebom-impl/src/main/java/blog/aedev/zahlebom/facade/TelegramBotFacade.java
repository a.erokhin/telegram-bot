package blog.aedev.zahlebom.facade;

import blog.aedev.zahlebom.dto.InviteDto;
import blog.aedev.zahlebom.dto.ListDto;
import blog.aedev.zahlebom.dto.ListInfoDto;
import blog.aedev.zahlebom.dto.UserDto;
import java.util.List;
import org.telegram.telegrambots.meta.api.objects.User;

public interface TelegramBotFacade {

    /**
     * Регистрируем пользователя, если пользователь уже существует возвращаем его.
     * Если передан токен, то присваиваем пользователю список, для которого создан токен.
     *
     * @param telegramUser  информация о пользователе telegram
     * @param tokenValue    значение токена
     * @return информация о пользователе
     */
    UserDto registerUser(User telegramUser, String tokenValue);

    ListDto getCurrentList();

    void cleanList();

    void deletePurchasedItems();

    InviteDto invite();

    InviteDto invite(Long listId);

    UserDto unsubscribe(Long listId, Long telegramRemovedUserId);

    List<ListInfoDto> getAllListInfo();

    ListInfoDto getListInfoById(Long listId);

    ListInfoDto createList();

    void deleteList(Long listId);

    void selectList(Long listId);

    ListDto markItemAsPurchasedAndGetList(Long itemId);

    ListDto deleteItem(Long itemId);

    void subscribe(Long listId);

    ListDto lastOperationCancel(Long listId);
}
