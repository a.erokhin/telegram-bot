package blog.aedev.zahlebom.config;

public interface ThymeleafConstants {

    String USER_ATTRIBUTE = "user";
    String BOT_NAME_ATTRIBUTE = "bot";
    String ITEMS_ATTRIBUTE = "items";
    String INVITE_URL_ATTRIBUTE = "invite";
    String LIST_ATTRIBUTE = "list";

    String WELCOME_TEMPLATE_1 = "welcome/1";
    String WELCOME_TEMPLATE_2 = "welcome/2";

    String TRAINING_TEMPLATE_1 = "training/1";
    String TRAINING_TEMPLATE_2 = "training/2";
    String TRAINING_TEMPLATE_3 = "training/3";
    String TRAINING_TEMPLATE_4 = "training/4";
    String TRAINING_TEMPLATE_5 = "training/5";
    String TRAINING_TEMPLATE_6 = "training/6";
    String TRAINING_TEMPLATE_7 = "training/7";
    String TRAINING_TEMPLATE_8 = "training/8";

    String ORDERED_LIST_TEMPLATE = "list/ordered";
    String EDITABLE_LIST_TEMPLATE = "list/editable";
    String SELECTABLE_LIST_TEMPLATE = "list/selectable";

    String CLEAN_TEMPLATE = "clean";
    String SHARE_TEMPLATE = "share";
    String INVITE_LINK_TEMPLATE = "invite_link";

    String LIST_TEMPLATE = "settings/list";
    String LIST_USERS_TEMPLATE = "settings/list_users";
    String SETTINGS_TEMPLATE = "settings/settings";
    String CREATE_LIST_TEMPLATE = "settings/create_list";
    String RENAME_TEMPLATE = "settings/rename";
    String RENAME_FINISH_TEMPLATE = "settings/rename_finish";
}
