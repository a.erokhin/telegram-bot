package blog.aedev.zahlebom.config;

import blog.aedev.zahlebom.repository.ThreadLocalStorage;
import java.time.OffsetDateTime;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider", dateTimeProviderRef = "auditingDateTimeProvider")
@RequiredArgsConstructor
public class PersistenceConfig {

    @Bean
    public AuditorAware<Long> auditorProvider() {
        return () -> Optional.of(ThreadLocalStorage.getUserId());
    }

    @Bean
    public DateTimeProvider auditingDateTimeProvider()  {
        return () -> Optional.of(OffsetDateTime.now());
    }
}
