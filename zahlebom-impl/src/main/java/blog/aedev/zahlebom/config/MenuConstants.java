package blog.aedev.zahlebom.config;

public interface MenuConstants {
    String SHOW_LIST_COMMAND = "\uD83D\uDCDD Показать список️";

    String CLEAN_LIST_COMMAND = "\\clean_list️";
    String CLEAR_LIST_COMMAND = "\\clear_list️";
    String SHOW_ORDERED_LIST = "\\show_ordered_list";
    String SHOW_SELECTABLE_LIST = "\\show_selectable_list";
    String SHOW_EDITABLE_LIST = "\\show_editable_list";

    String REMOVE_ITEM_WITH_ITEM_ID_COMMAND = "\\remove_item?item_id=%s";
    String SELECT_ITEM_WITH_ITEM_ID_COMMAND = "\\select_item?item_id=%s";
    String REMOVE_ITEM_COMMAND = "\\remove_item";
    String SELECT_ITEM_COMMAND = "\\select_item";

    String SHARE_COMMAND = "\uD83D\uDC6A Поделиться";
    String SHARE_WITH_LIST_ID_COMMAND = "\uD83D\uDC6A Поделиться?list_id=%s";
    String SHOW_LIST_USERS_COMMAND = "\\show_list_users";
    String SHOW_LIST_USERS_WITH_LIST_ID_COMMAND = "\\show_list_users?list_id=%s";

    String OPERATION_CANCEL_COMMAND = "\\operation_cancel";
    String OPERATION_CANCEL_COMMAND_WITH_LIST_ID = "\\operation_cancel?list_id=%s";

    String SETTINGS_COMMAND = "⚙️ Настройки";

    String EDIT_LIST_WITH_LIST_ID_COMMAND = "\\edit_list?list_id=%s";
    String EDIT_LIST_COMMAND = "\\edit_list";

    String RENAME_LIST_WITH_LIST_ID_COMMAND = "\\rename_list?list_id=%s";
    String RENAME_LIST_COMMAND = "\\rename_list";

    String DELETE_LIST_WITH_LIST_ID_COMMAND = "\\delete_list?list_id=%s";
    String DELETE_LIST_COMMAND = "\\delete_list";

    String SELECT_LIST_WITH_LIST_ID_COMMAND = "\\select_list?list_id=%s";
    String SELECT_LIST_COMMAND = "\\select_list";

    String UNSUBSCRIBE_COMMAND = "\\unsubscribe";
    String UNSUBSCRIBE_WITH_LIST_ID_AND_USER_ID_COMMAND = "\\unsubscribe?list_id=%s&user_id=%s";

    String CREATE_LIST_COMMAND = "\\create_list";

    String START_COMMAND = "start";
    String TRAINING_COMMAND = "\\training";
}
