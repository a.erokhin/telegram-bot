package blog.aedev.zahlebom.config;

public interface MainConstants {
    String MESSAGE_ID = "message_id";
    String ITEM_ID = "item_id";
    String LIST_ID = "list_id";
}
