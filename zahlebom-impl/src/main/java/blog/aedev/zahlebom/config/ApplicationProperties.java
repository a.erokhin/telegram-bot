package blog.aedev.zahlebom.config;

import java.time.Duration;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
@Getter
@Setter
public class ApplicationProperties {

    private final TelegramBot telegramBot = new TelegramBot();

    private final Token token = new Token();

    @Getter
    @Setter
    public static class TelegramBot {
        private String username;
        private String token;
    }

    @Getter
    @Setter
    public static class Token {
        private int length;
        private Duration expiration;
    }
}
