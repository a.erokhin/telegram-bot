package blog.aedev.zahlebom;

import blog.aedev.zahlebom.config.ApplicationProperties;
import blog.aedev.zahlebom.config.DefaultProfileUtil;
import java.net.InetAddress;
import java.net.UnknownHostException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties({ApplicationProperties.class})
public class ShoppinglistApplication {

	private final Environment env;

	public ShoppinglistApplication(Environment env) {
		this.env = env;
	}

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(ShoppinglistApplication.class);
		DefaultProfileUtil.addDefaultProfile(app);
		Environment env = app.run(args).getEnvironment();
		logApplicationStartup(env);
	}

	private static void logApplicationStartup(Environment env) {
		String protocol = "http";
		if (env.getProperty("server.ssl.key-store") != null) {
			protocol = "https";
		}
		String serverPort = env.getProperty("server.port");
		String contextPath = env.getProperty("server.servlet.context-path");
		String botName = env.getProperty("application.telegram-bot.username");

		String hostAddress = "localhost";
		try {
			hostAddress = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			log.warn("The host name could not be determined, using `localhost` as fallback");
		}
		log.info("\n----------------------------------------------------------\n\t" +
				"Application '{}' is running! Access URLs:\n\t" +
				"Local: \t\t{}://localhost:{}{}\n\t" +
				"External: \t{}://{}:{}{}\n\t" +
				"Bot: \t\thttps://t.me/{}\n\t" +
				"Profile(s): \t{}\n----------------------------------------------------------",
			env.getProperty("spring.application.name"),
			protocol,
			serverPort,
			contextPath,
			protocol,
			hostAddress,
			serverPort,
			contextPath,
			botName,
			env.getActiveProfiles());

		String configServerStatus = env.getProperty("configserver.status");
		if (configServerStatus == null) {
			configServerStatus = "Not found or not setup for this application";
		}
		log.info("\n----------------------------------------------------------\n\t" +
			"Config Server: \t{}\n----------------------------------------------------------", configServerStatus);
	}
}
