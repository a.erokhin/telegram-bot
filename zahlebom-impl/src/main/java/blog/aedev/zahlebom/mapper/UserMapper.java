package blog.aedev.zahlebom.mapper;

import blog.aedev.zahlebom.config.MapperDefaultConfig;
import blog.aedev.zahlebom.dto.UserDto;
import blog.aedev.zahlebom.model.UserInfo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.telegram.telegrambots.meta.api.objects.User;

@Mapper(config = MapperDefaultConfig.class)
public interface UserMapper {

    @Mapping(source = "new", target = "isNew")
    UserDto map(UserInfo userInfo);

    @Mapping(source = "userName", target = "username")
    @Mapping(ignore = true, target = "selectList")
    @Mapping(ignore = true, target = "isNew")
    @Mapping(expression = "java(true)", target = "enabled")
    @Mapping(ignore = true, target = "audit")
    UserInfo map(User telegramUser);
}
