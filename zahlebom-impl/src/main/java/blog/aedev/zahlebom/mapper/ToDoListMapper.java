package blog.aedev.zahlebom.mapper;

import blog.aedev.zahlebom.config.MapperDefaultConfig;
import blog.aedev.zahlebom.dto.ListDto;
import blog.aedev.zahlebom.dto.ListInfoDto;
import blog.aedev.zahlebom.model.ToDoList;
import blog.aedev.zahlebom.repository.ThreadLocalStorage;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapperDefaultConfig.class, uses = {ItemMapper.class, UserMapper.class})
public interface ToDoListMapper {

    @Mapping(target = "isCurrentUserOwner", expression = "java(isCurrentUserOwner(list))")
    @Mapping(target = "isSelected", source = "selectForCurrentUser")
    @Mapping(target = "userId", expression = "java(getCurrentUserId())")
    ListInfoDto mapToListInfoDto(ToDoList list);

    List<ListInfoDto> mapToListInfoDto(List<ToDoList> list);

    ListDto mapToListDto(ToDoList list);

    default boolean isCurrentUserOwner(ToDoList list) {
        return list.getOwner().getId().equals(ThreadLocalStorage.getUserId());
    }

    default Long getCurrentUserId() {
        return ThreadLocalStorage.getUserId();
    }
}
