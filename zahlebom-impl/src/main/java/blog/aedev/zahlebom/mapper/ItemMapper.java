package blog.aedev.zahlebom.mapper;

import blog.aedev.zahlebom.config.MapperDefaultConfig;
import blog.aedev.zahlebom.dto.ItemDto;
import blog.aedev.zahlebom.model.DeleteItem;
import blog.aedev.zahlebom.model.Item;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapperDefaultConfig.class)
public interface ItemMapper {

    @Mapping(source = "product.name", target = "productName")
    @Mapping(source = "list.id", target = "listId")
    @Mapping(source = "purchased", target = "isPurchased")
    ItemDto map(Item item);

    List<ItemDto> map(List<Item> item);

    @Mapping(source = "audit.createdBy", target = "createdBy")
    @Mapping(source = "audit.createdDate", target = "createdDate")
    @Mapping(source = "audit.lastModifiedBy", target = "lastModifiedBy")
    @Mapping(source = "audit.lastModifiedDate", target = "lastModifiedDate")
    @Mapping(ignore = true, target = "deletedBy")
    @Mapping(ignore = true, target = "deletedDate")
    DeleteItem mapToDeleteItem(Item item);

    List<DeleteItem> mapToDeleteItem(List<Item> item);

    @Mapping(source = "createdBy", target = "audit.createdBy")
    @Mapping(source = "createdDate", target = "audit.createdDate")
    @Mapping(ignore = true, target = "audit.lastModifiedBy")
    @Mapping(ignore = true, target = "audit.lastModifiedDate")
    Item map(DeleteItem deleteItem);
}
